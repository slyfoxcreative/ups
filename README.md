# slyfoxcreative/ups

Library for the UPS Rating API.

```php
<?php

require 'vendor/autoload.php';

use SlyFoxCreative\Ups\Address;
use SlyFoxCreative\Ups\Client;
use SlyFoxCreative\Ups\Dimensions;
use SlyFoxCreative\Ups\Entity;
use SlyFoxCreative\Ups\Package;
use SlyFoxCreative\Ups\Request;
use SlyFoxCreative\Ups\Shipper;

$client = new Client(
    username: 'test',
    password: 'test',
    accessKey: 'test',
);

$origin = new Address(
    city: 'Greenwood',
    state: 'IN',
    postalCode: '46143',
    country: 'US',
);

$destination = new Address(
    city: 'Indianapolis',
    state: 'IN',
    postalCode: '46227',
    country: 'US',
    residential: true,
);

$shipper = new Shipper(
    name: 'Excel',
    address: $origin,
    number: 'test',
);

$shipTo = new Entity(name: 'Joe Bloggs', address: $destination);

$shipFrom = new Entity(name: 'Excel', address: $origin);

$package = new Package(
    weight: '10',
    dimensions: new Dimensions(
        length: '10',
        width: '5',
        height: '5',
    ),
);

$request = new Request(
    shipper: $shipper,
    shipTo: $shipTo,
    shipFrom: $shipFrom,
    packages: [$package],
);

$response = $client->request($request);

$response->rates()->each(function ($rate) {
    echo $rate->serviceCode() . ': ' . $rate->cost() . "\n";
});
```
