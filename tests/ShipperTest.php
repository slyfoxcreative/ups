<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Address;
use SlyFoxCreative\Ups\Shipper;

class ShipperTest extends TestCase
{
    public function testToArray(): void
    {
        $address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
        );

        $shipper = new Shipper(
            name: 'Excel',
            address: $address,
            number: '12345',
        );

        $expected = [
            'Name' => 'Excel',
            'Address' => [
                'City' => 'Greenwood',
                'StateProvinceCode' => 'IN',
                'PostalCode' => '46143',
                'CountryCode' => 'US',
            ],
            'ShipperNumber' => '12345',
        ];

        self::assertSame($expected, $shipper->toArray());
    }
}
