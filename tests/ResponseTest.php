<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Rate;
use SlyFoxCreative\Ups\Response;

class ResponseTest extends TestCase
{
    public function testRates(): void
    {
        $response = new Response([
            'RateResponse' => [
                'RatedShipment' => [
                    [
                        'Service' => [
                            'Code' => '01',
                        ],
                        'NegotiatedRateCharges' => [
                            'TotalCharge' => [
                                'MonetaryValue' => '1',
                            ],
                        ],
                        'TotalCharges' => [
                            'MonetaryValue' => '2',
                        ],
                    ],
                ],
            ],
        ]);

        self::assertEquals(collect([new Rate('01', '1')]), $response->rates());
    }
}
