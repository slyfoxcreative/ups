<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Address;

class AddressTest extends TestCase
{
    public function testToArray(): void
    {
        $address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
        );

        $expected = [
            'City' => 'Greenwood',
            'StateProvinceCode' => 'IN',
            'PostalCode' => '46143',
            'CountryCode' => 'US',
        ];

        self::assertSame($expected, $address->toArray());
    }

    public function testToArrayWithResidential(): void
    {
        $address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
            residential: true,
        );

        $expected = [
            'City' => 'Greenwood',
            'StateProvinceCode' => 'IN',
            'PostalCode' => '46143',
            'CountryCode' => 'US',
            'ResidentialAddressIndicator' => '',
        ];

        self::assertSame($expected, $address->toArray());
    }

    public function testState(): void
    {
        $values = [
            ['Alabama', 'AL'],
            ['Alaska', 'AK'],
            ['Arizona', 'AZ'],
            ['Arkansas', 'AR'],
            ['California', 'CA'],
            ['Colorado', 'CO'],
            ['Connecticut', 'CT'],
            ['Delaware', 'DE'],
            ['District Of Columbia', 'DC'],
            ['Florida', 'FL'],
            ['Georgia', 'GA'],
            ['Hawaii', 'HI'],
            ['Idaho', 'ID'],
            ['Illinois', 'IL'],
            ['Indiana', 'IN'],
            ['Iowa', 'IA'],
            ['Kansas', 'KS'],
            ['Kentucky', 'KY'],
            ['Louisiana', 'LA'],
            ['Maine', 'ME'],
            ['Maryland', 'MD'],
            ['Massachusetts', 'MA'],
            ['Michigan', 'MI'],
            ['Minnesota', 'MN'],
            ['Mississippi', 'MS'],
            ['Missouri', 'MO'],
            ['Montana', 'MT'],
            ['Nebraska', 'NE'],
            ['Nevada', 'NV'],
            ['New Hampshire', 'NH'],
            ['New Jersey', 'NJ'],
            ['New Mexico', 'NM'],
            ['New York', 'NY'],
            ['North Carolina', 'NC'],
            ['North Dakota', 'ND'],
            ['Ohio', 'OH'],
            ['Oklahoma', 'OK'],
            ['Oregon', 'OR'],
            ['Pennsylvania', 'PA'],
            ['Rhode Island', 'RI'],
            ['South Carolina', 'SC'],
            ['South Dakota', 'SD'],
            ['Tennessee', 'TN'],
            ['Texas', 'TX'],
            ['Utah', 'UT'],
            ['Vermont', 'VT'],
            ['Virginia', 'VA'],
            ['Washington', 'WA'],
            ['West Virginia', 'WV'],
            ['Wisconsin', 'WI'],
            ['Wyoming', 'WY'],
            ['American Samoa', 'AS'],
            ['Guam', 'GU'],
            ['Northern Mariana Islands', 'MP'],
            ['Puerto Rico', 'PR'],
            ['U.S. Virgin Islands', 'VI'],
            ['Alberta', 'AB'],
            ['British Columbia', 'BC'],
            ['Manitoba', 'MB'],
            ['New Brunswick', 'NB'],
            ['Newfoundland And Labrador', 'NL'],
            ['Nova Scotia', 'NS'],
            ['Ontario', 'ON'],
            ['Prince Edward Island', 'PE'],
            ['Quebec', 'QC'],
            ['Saskatchewan', 'SK'],
        ];

        foreach ($values as [$baseValue, $expectedValue]) {
            foreach ([$baseValue, mb_strtoupper($baseValue), mb_strtolower($baseValue)] as $value) {
                $address = new Address(
                    city: 'Greenwood',
                    state: $value,
                    postalCode: '46143',
                    country: 'US',
                );

                $expected = [
                    'City' => 'Greenwood',
                    'StateProvinceCode' => $expectedValue,
                    'PostalCode' => '46143',
                    'CountryCode' => 'US',
                ];

                self::assertSame($expected, $address->toArray());
            }
        }
    }

    public function testUnknownState(): void
    {
        $address = new Address(
            city: 'Greenwood',
            state: 'Test',
            postalCode: '46143',
            country: 'US',
        );

        $expected = [
            'City' => 'Greenwood',
            'StateProvinceCode' => 'Test',
            'PostalCode' => '46143',
            'CountryCode' => 'US',
        ];

        self::assertSame($expected, $address->toArray());
    }
}
