<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Rate;

class RateTest extends TestCase
{
    public function testServiceCode(): void
    {
        $rate = new Rate('03', '10');

        self::assertSame('03', $rate->serviceCode());
    }

    public function testCost(): void
    {
        $rate = new Rate('03', '10');

        self::assertSame(10.0, $rate->cost());
    }
}
