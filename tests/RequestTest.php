<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Address;
use SlyFoxCreative\Ups\Entity;
use SlyFoxCreative\Ups\Package;
use SlyFoxCreative\Ups\Request;
use SlyFoxCreative\Ups\Shipper;

class RequestTest extends TestCase
{
    protected Address $address;

    protected Shipper $shipper;

    protected Entity $entity;

    protected package $package;

    protected function setUp(): void
    {
        parent::setUp();

        $this->address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
        );

        $this->shipper = new Shipper(
            name: 'Alice',
            address: $this->address,
            number: '12345',
        );

        $this->entity = new Entity(
            name: 'Bob',
            address: $this->address,
        );

        $this->package = new Package(weight: '3');
    }

    public function testToArray(): void
    {
        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
        );

        /** @var RequestData */
        $expected = [
            'RateRequest' => [
                'PickupType' => [
                    'Code' => '01',
                ],
                'Shipment' => [
                    'Shipper' => [
                        'Name' => 'Alice',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                        'ShipperNumber' => '12345',
                    ],
                    'ShipTo' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'ShipFrom' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'Package' => [
                        [
                            'PackagingType' => [
                                'Code' => '00',
                            ],
                            'PackageWeight' => [
                                'UnitOfMeasurement' => [
                                    'Code' => 'LBS',
                                ],
                                'Weight' => '3',
                            ],
                        ],
                    ],
                    'ShipmentRatingOptions' => [
                        'NegotiatedRatesIndicator' => '',
                    ],
                ],
            ],
        ];

        self::assertSame($expected, $request->toArray());
    }

    public function testNonNegotiatedRequest(): void
    {
        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
            negotiated: false,
        );

        /** @var RequestData */
        $expected = [
            'RateRequest' => [
                'PickupType' => [
                    'Code' => '01',
                ],
                'Shipment' => [
                    'Shipper' => [
                        'Name' => 'Alice',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                        'ShipperNumber' => '12345',
                    ],
                    'ShipTo' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'ShipFrom' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'Package' => [
                        [
                            'PackagingType' => [
                                'Code' => '00',
                            ],
                            'PackageWeight' => [
                                'UnitOfMeasurement' => [
                                    'Code' => 'LBS',
                                ],
                                'Weight' => '3',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        self::assertSame($expected, $request->toArray());
    }
}
