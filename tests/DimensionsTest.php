<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Dimensions;

class DimensionsTest extends TestCase
{
    public function testLengthValidation(): void
    {
        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Assertion failed: Dimensions length must be numeric');

        new Dimensions(length: 'a', width: '1.0', height: '1.0');
    }

    public function testWidthValidation(): void
    {
        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Assertion failed: Dimensions width must be numeric');

        new Dimensions(length: '1.0', width: 'a', height: '1.0');
    }

    public function testHeightValidation(): void
    {
        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Assertion failed: Dimensions height must be numeric');

        new Dimensions(length: '1.0', width: '1.0', height: 'a');
    }
}
