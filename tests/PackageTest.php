<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Dimensions;
use SlyFoxCreative\Ups\Package;

class PackageTest extends TestCase
{
    public function testWeightValidation(): void
    {
        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Assertion failed: Package weight must be numeric');

        new Package(weight: 'a');
    }

    public function testToArray(): void
    {
        $package = new Package(weight: '3');

        $expected = [
            'PackagingType' => [
                'Code' => '00',
            ],
            'PackageWeight' => [
                'UnitOfMeasurement' => [
                    'Code' => 'LBS',
                ],
                'Weight' => '3',
            ],
        ];

        self::assertSame($expected, $package->toArray());
    }

    public function testToArrayWithDimensions(): void
    {
        $package = new Package(
            weight: '3',
            dimensions: new Dimensions(
                length: '10',
                width: '5',
                height: '5',
            ),
        );

        $expected = [
            'PackagingType' => [
                'Code' => '00',
            ],
            'PackageWeight' => [
                'UnitOfMeasurement' => [
                    'Code' => 'LBS',
                ],
                'Weight' => '3',
            ],
            'Dimensions' => [
                'UnitOfMeasurement' => [
                    'Code' => 'IN',
                ],
                'Length' => '10',
                'Width' => '5',
                'Height' => '5',
            ],
        ];

        self::assertSame($expected, $package->toArray());
    }
}
