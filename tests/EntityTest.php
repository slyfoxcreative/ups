<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Ups\Address;
use SlyFoxCreative\Ups\Entity;

class EntityTest extends TestCase
{
    public function testToArray(): void
    {
        $address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
        );

        $entity = new Entity(
            name: 'Excel',
            address: $address,
        );

        $expected = [
            'Name' => 'Excel',
            'Address' => [
                'City' => 'Greenwood',
                'StateProvinceCode' => 'IN',
                'PostalCode' => '46143',
                'CountryCode' => 'US',
            ],
        ];

        self::assertSame($expected, $entity->toArray());
    }
}
