<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Tests;

use BlastCloud\Guzzler\UsesGuzzler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use SlyFoxCreative\Ups\Address;
use SlyFoxCreative\Ups\Client;
use SlyFoxCreative\Ups\Entity;
use SlyFoxCreative\Ups\Exception\RequestException;
use SlyFoxCreative\Ups\Package;
use SlyFoxCreative\Ups\Rate;
use SlyFoxCreative\Ups\Request;
use SlyFoxCreative\Ups\Shipper;

class ClientTest extends TestCase
{
    use UsesGuzzler;

    /** @var RequestData */
    protected array $json;

    protected Address $address;

    protected Shipper $shipper;

    protected Entity $entity;

    protected Package $package;

    protected function setUp(): void
    {
        $this->json = [
            'RateRequest' => [
                'PickupType' => [
                    'Code' => '01',
                ],
                'Shipment' => [
                    'Shipper' => [
                        'Name' => 'Alice',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                        'ShipperNumber' => '12345',
                    ],
                    'ShipTo' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'ShipFrom' => [
                        'Name' => 'Bob',
                        'Address' => [
                            'City' => 'Greenwood',
                            'StateProvinceCode' => 'IN',
                            'PostalCode' => '46143',
                            'CountryCode' => 'US',
                        ],
                    ],
                    'Package' => [
                        [
                            'PackagingType' => [
                                'Code' => '00',
                            ],
                            'PackageWeight' => [
                                'UnitOfMeasurement' => [
                                    'Code' => 'LBS',
                                ],
                                'Weight' => '3',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->address = new Address(
            city: 'Greenwood',
            state: 'IN',
            postalCode: '46143',
            country: 'US',
        );

        $this->shipper = new Shipper(
            name: 'Alice',
            address: $this->address,
            number: '12345',
        );

        $this->entity = new Entity(
            name: 'Bob',
            address: $this->address,
        );

        $this->package = new Package(weight: '3');
    }

    public function testSuccess(): void
    {
        $this->json['RateRequest']['Shipment']['ShipmentRatingOptions'] = [
            'NegotiatedRatesIndicator' => '',
        ];

        $this->guzzler
            ->expects($this->once())
            ->post('https://wwwcie.ups.com/ship/v1801/rating/Shop')
            ->withHeaders([
                'transId' => 'TEST',
                'transactionSrc' => 'slyfoxcreative/ups',
                'AccessLicenseNumber' => 'test',
                'Username' => 'test',
                'Password' => 'test',
            ])
            ->withJson($this->json)
            ->willRespond(new Response(200, [], $this->fixture('response')))
        ;

        $client = new Client(
            username: 'test',
            password: 'test',
            accessKey: 'test',
            sandbox: true,
            guzzler: $this->guzzler,
        );

        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
        );

        $response = $client->request($request);

        $expected = collect([
            new Rate('01', '39.62'),
            new Rate('02', '25.03'),
            new Rate('03', '14.05'),
            new Rate('12', '17.99'),
            new Rate('13', '37.47'),
            new Rate('14', '69.77'),
        ]);

        self::assertEquals($expected, $response->rates());
    }

    public function testFailure(): void
    {
        $this->json['RateRequest']['Shipment']['ShipmentRatingOptions'] = [
            'NegotiatedRatesIndicator' => '',
        ];

        $this->guzzler
            ->expects($this->once())
            ->post('https://wwwcie.ups.com/ship/v1801/rating/Shop')
            ->withHeaders([
                'transId' => 'TEST',
                'transactionSrc' => 'slyfoxcreative/ups',
                'AccessLicenseNumber' => 'test',
                'Username' => 'test',
                'Password' => 'test',
            ])
            ->withJson($this->json)
            ->willRespond(new Response(400, [], $this->fixture('error')))
        ;

        self::expectException(RequestException::class);
        self::expectExceptionMessage('The requested service is unavailable between the selected locations.');

        $client = new Client(
            username: 'test',
            password: 'test',
            accessKey: 'test',
            sandbox: true,
            guzzler: $this->guzzler,
        );

        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
        );

        $response = $client->request($request);
    }

    public function testLogging(): void
    {
        $this->json['RateRequest']['Shipment']['ShipmentRatingOptions'] = [
            'NegotiatedRatesIndicator' => '',
        ];

        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
        );

        $logger = \Mockery::mock(LoggerInterface::class);
        $logger->expects()->debug(var_export($request->toArray(), true))->once();
        $logger->expects()->debug(var_export(json_decode($this->fixture('response'), true), true))->once();

        $this->guzzler
            ->expects($this->once())
            ->post('https://wwwcie.ups.com/ship/v1801/rating/Shop')
            ->withHeaders([
                'transId' => 'TEST',
                'transactionSrc' => 'slyfoxcreative/ups',
                'AccessLicenseNumber' => 'test',
                'Username' => 'test',
                'Password' => 'test',
            ])
            ->withJson($this->json)
            ->willRespond(new Response(200, [], $this->fixture('response')))
        ;

        $client = new Client(
            username: 'test',
            password: 'test',
            accessKey: 'test',
            sandbox: true,
            guzzler: $this->guzzler,
            logger: $logger,
        );

        $response = $client->request($request);
    }

    public function testNonNegotiatedRequest(): void
    {
        $this->guzzler
            ->expects($this->once())
            ->post('https://wwwcie.ups.com/ship/v1801/rating/Shop')
            ->withHeaders([
                'transId' => 'TEST',
                'transactionSrc' => 'slyfoxcreative/ups',
                'AccessLicenseNumber' => 'test',
                'Username' => 'test',
                'Password' => 'test',
            ])
            ->withJson($this->json)
            ->willRespond(new Response(200, [], $this->fixture('non_negotiated_response')))
        ;

        $client = new Client(
            username: 'test',
            password: 'test',
            accessKey: 'test',
            sandbox: true,
            guzzler: $this->guzzler,
        );

        $request = new Request(
            shipper: $this->shipper,
            shipFrom: $this->entity,
            shipTo: $this->entity,
            packages: [$this->package],
            negotiated: false,
        );

        $response = $client->request($request);

        $expected = collect([
            new Rate('01', '39.35'),
            new Rate('02', '23.24'),
            new Rate('03', '15.24'),
            new Rate('12', '20.20'),
            new Rate('13', '40.60'),
            new Rate('14', '86.79'),
        ]);

        self::assertEquals($expected, $response->rates());
    }

    // This test is just making sure the function call doesn't throw an
    // exception, so it has no useful assertions to perform.
    #[DoesNotPerformAssertions]
    public function testVerifyResponse(): void
    {
        $client = new Client(
            username: 'test',
            password: 'test',
            accessKey: 'test',
            sandbox: true,
            guzzler: $this->guzzler,
        );

        $response = json_decode($this->fixture('response'), true);

        $client->verifyResponse($response);
    }

    private function fixture(string $file): string
    {
        $contents = file_get_contents(__DIR__ . "/fixtures/{$file}.json");

        if ($contents === false) {
            throw new \Exception("Failed to read {$file}");
        }

        return $contents;
    }
}
