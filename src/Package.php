<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

use function SlyFoxCreative\Utilities\assert_true;

class Package
{
    public function __construct(
        private string $weight,
        private ?Dimensions $dimensions = null,
    ) {
        assert_true(is_numeric($weight), 'Package weight must be numeric');
    }

    /** @return PackageData  */
    public function toArray(): array
    {
        $array = [
            'PackagingType' => [
                'Code' => '00',
            ],
            'PackageWeight' => [
                'UnitOfMeasurement' => [
                    'Code' => 'LBS',
                ],
                'Weight' => $this->weight,
            ],
        ];

        if (!is_null($this->dimensions)) {
            $array['Dimensions'] = [
                'UnitOfMeasurement' => [
                    'Code' => 'IN',
                ],
                'Length' => $this->dimensions->length,
                'Width' => $this->dimensions->width,
                'Height' => $this->dimensions->height,
            ];
        }

        return $array;
    }
}
