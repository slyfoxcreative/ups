<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

class Address
{
    private string $state;

    public function __construct(
        private string $city,
        string $state,
        private string $postalCode,
        private string $country,
        private bool $residential = false,
    ) {
        $this->state = state_code($state);
    }

    /** @return AddressData */
    public function toArray(): array
    {
        $array = [
            'City' => $this->city,
            'StateProvinceCode' => $this->state,
            'PostalCode' => $this->postalCode,
            'CountryCode' => $this->country,
        ];

        if ($this->residential) {
            $array['ResidentialAddressIndicator'] = '';
        }

        return $array;
    }
}
