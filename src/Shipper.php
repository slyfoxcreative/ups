<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

class Shipper extends Entity
{
    public function __construct(
        string $name,
        Address $address,
        private string $number,
    ) {
        parent::__construct($name, $address);
    }

    /** @return array<string, array<string, string>|string> */
    public function toArray(): array
    {
        $array = parent::toArray();
        $array['ShipperNumber'] = $this->number;

        return $array;
    }
}
