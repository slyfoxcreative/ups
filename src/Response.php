<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

use Illuminate\Support\Collection;

class Response
{
    /** @param ResponseData $data */
    public function __construct(private array $data) {}

    /** @return Collection<int, Rate> */
    public function rates(): Collection
    {
        $rates = $this->data['RateResponse']['RatedShipment'];

        return collect($rates)
            ->map(function ($rate) {
                $cost = isset($rate['NegotiatedRateCharges'])
                    ? $rate['NegotiatedRateCharges']['TotalCharge']['MonetaryValue']
                    : $rate['TotalCharges']['MonetaryValue'];

                return new Rate($rate['Service']['Code'], $cost);
            })
            ->sortBy(function ($rate) {
                return $rate->serviceCode();
            })
            ->values()
        ;
    }
}
