<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

use BlastCloud\Guzzler\Guzzler;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use SlyFoxCreative\Ups\Exception\RequestException;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_string;

class Client
{
    private bool $mock;

    private GuzzleClient $client;

    public function __construct(
        private string $username,
        private string $password,
        private string $accessKey,
        private ?LoggerInterface $logger = null,
        bool $sandbox = false,
        ?Guzzler $guzzler = null,
    ) {
        $baseUri = $sandbox
            ? 'https://wwwcie.ups.com'
            : 'https://onlinetools.ups.com';

        $this->client = ! is_null($guzzler)
            ? $guzzler->getClient(['base_uri' => $baseUri])
            : new GuzzleClient(['base_uri' => $baseUri]);

        $this->mock = ! is_null($guzzler);
    }

    public function request(Request $request): Response
    {
        $options = [
            'headers' => [
                'transId' => $this->mock ? 'TEST' : bin2hex(random_bytes(16)),
                'transactionSrc' => 'slyfoxcreative/ups',
                'AccessLicenseNumber' => $this->accessKey,
                'Username' => $this->username,
                'Password' => $this->password,
            ],
            'json' => $request->toArray(),
        ];

        if (isset($this->logger)) {
            $this->logger->debug(var_export($request->toArray(), true));
        }

        try {
            $response = $this->client->request('POST', '/ship/v1801/rating/Shop', $options);
        } catch (ClientException $exception) {
            if (isset($this->logger)) {
                $this->logger->debug((string) $exception->getResponse()->getBody());
            }

            throw new RequestException($exception);
        }

        $response = json_decode((string) $response->getBody(), true);
        $this->verifyResponse($response);

        if (isset($this->logger)) {
            $this->logger->debug(var_export($response, true));
        }

        return new Response($response);
    }

    /**
     * @phpstan-assert ResponseData $response
     */
    public function verifyResponse(mixed $response): void
    {
        assert_array($response);
        assert_array($response['RateResponse']);
        assert_array($response['RateResponse']['RatedShipment']);
        foreach ($response['RateResponse']['RatedShipment'] as $rate) {
            assert_array($rate['Service']);
            assert_string($rate['Service']['Code']);
            if (isset($rate['NegotiatedRateCharges'])) {
                assert_array($rate['NegotiatedRateCharges']);
                assert_array($rate['NegotiatedRateCharges']['TotalCharge']);
                assert_string($rate['NegotiatedRateCharges']['TotalCharge']['MonetaryValue']);
            }
            assert_array($rate['TotalCharges']);
            assert_string($rate['TotalCharges']['MonetaryValue']);
        }
    }
}
