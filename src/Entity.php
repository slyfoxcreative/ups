<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

class Entity
{
    public function __construct(
        private string $name,
        private Address $address,
    ) {}

    /** @return array<string, array<string, string>|string> */
    public function toArray(): array
    {
        return [
            'Name' => $this->name,
            'Address' => $this->address->toArray(),
        ];
    }
}
