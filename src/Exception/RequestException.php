<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups\Exception;

use GuzzleHttp\Exception\ClientException;

use function SlyFoxCreative\Utilities\assert_array;

class RequestException extends \Exception
{
    private string $uri;

    private string $body;

    public function __construct(ClientException $exception)
    {
        $this->uri = (string) $exception->getRequest()->getUri();
        $this->body = (string) $exception->getResponse()->getBody();

        $json = json_decode($this->body, true);
        assert_array($json);
        assert_array($json['response']);
        assert_array($json['response']['errors']);
        assert_array($json['response']['errors'][0]);
        $error = $json['response']['errors'][0];

        parent::__construct($error['message'], (int) $error['code']);
    }

    public function uri(): string
    {
        return $this->uri;
    }

    public function body(): string
    {
        return $this->body;
    }
}
