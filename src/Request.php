<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

use Illuminate\Support\Collection;

class Request
{
    /** @var Collection<int, Package> */
    private Collection $packages;

    /** @param array<int, Package>|Collection<int, Package> $packages */
    public function __construct(
        private Shipper $shipper,
        private Entity $shipTo,
        private Entity $shipFrom,
        array|Collection $packages,
        private bool $negotiated = true,
    ) {
        $this->packages = is_array($packages) ? collect($packages) : $packages;
    }

    /** @return RequestData */
    public function toArray(): array
    {
        $array = [
            'RateRequest' => [
                'PickupType' => [
                    'Code' => '01',
                ],
                'Shipment' => [
                    'Shipper' => $this->shipper->toArray(),
                    'ShipTo' => $this->shipTo->toArray(),
                    'ShipFrom' => $this->shipFrom->toArray(),
                    'Package' => $this->packages->map->toArray()->toArray(),
                ],
            ],
        ];

        if ($this->negotiated) {
            $array['RateRequest']['Shipment']['ShipmentRatingOptions'] = [
                'NegotiatedRatesIndicator' => '',
            ];
        }

        return $array;
    }
}
