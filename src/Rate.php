<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

class Rate
{
    public function __construct(
        private string $serviceCode,
        private string $cost,
    ) {}

    public function serviceCode(): string
    {
        return $this->serviceCode;
    }

    public function cost(): float
    {
        return (float) $this->cost;
    }
}
