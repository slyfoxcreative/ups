<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ups;

use function SlyFoxCreative\Utilities\assert_true;

class Dimensions
{
    public function __construct(
        public readonly string $length,
        public readonly string $width,
        public readonly string $height,
    ) {
        assert_true(is_numeric($length), 'Dimensions length must be numeric');
        assert_true(is_numeric($width), 'Dimensions width must be numeric');
        assert_true(is_numeric($height), 'Dimensions height must be numeric');
    }
}
